function init() {

// 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el
// evento click que ejecute un console log con la información del evento del click
$$button = document.createElement('button');
document.body.appendChild($$button);
$$button.id = 'btnToClick';
document.getElementById('btnToClick').innerHTML = 'Soy un botón';

$$button.addEventListener('click', eventClick)

function eventClick() {
    alert('En consola te digo qué hago');
    console.log('creo un alert cuando pulso el botón');
}

// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.
const $$focus = document.querySelector('.focus');
$$focus.addEventListener('focus',inputFocus);

function inputFocus(capturarValor){
    let text = capturarValor.target.value;
    console.log(text);
    console.log($$focus)
}

// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.
const $$input = document.querySelector('.value');
$$input.addEventListener('input', textoForm);

function textoForm(capturarValor){
    let text = capturarValor.target.value;
    console.log(text);
}


}
window.onload = init;

